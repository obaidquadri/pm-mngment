﻿using LibraryManagement.Data.Interfaces;
using LibraryManagement.Data.Model;
using LibraryManagement.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Controllers
{
    public class TenantController : Controller
    {
        private readonly ITenantRepository _tenantRepository;
        private readonly IPropRepository _propRepository;

        public TenantController(ITenantRepository tenantRepository, IPropRepository propRepository)
        {
            _tenantRepository = tenantRepository;
            _propRepository = propRepository;
        }
        [Route("Tenant")]
        public IActionResult List()
        {
            var tenantVM = new List<TenantViewModel>();

            var tenants = _tenantRepository.GetAll();

            if (tenants.Count() == 0)
            {
                return View("Empty");
            }

            foreach (var tenant in tenants)
            {
                tenantVM.Add(new TenantViewModel
                {
                    Tenant = tenant,
                    PropCount = _propRepository.Count(x => x.OccuId == tenant.TenantId)
                });
            }

            return View(tenantVM);
        }

        public IActionResult Delete(int id)
        {
            var tenant = _tenantRepository.GetById(id);

            _tenantRepository.Delete(tenant);

            return RedirectToAction("List");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Tenant tenant)
        {
            if (!ModelState.IsValid)
            {
                return View(tenant);
            }

            _tenantRepository.Create(tenant);

            return RedirectToAction("List");
        }

        public IActionResult Update(int id)
        {
            var tenant = _tenantRepository.GetById(id);

            return View(tenant);
        }

        [HttpPost]
        public IActionResult Update(Tenant tenant)
        {
            if (!ModelState.IsValid)
            {
                return View(tenant);
            }

            _tenantRepository.Update(tenant);

            return RedirectToAction("List");
        }
    }
}
