﻿using LibraryManagement.Data.Interfaces;
using LibraryManagement.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Controllers
{
    public class RentController : Controller
    {
        private readonly IPropRepository _bookRepository;
        private readonly ITenantRepository _customerRepository;

        public RentController(IPropRepository bookRepository, ITenantRepository customerRepository)
        {
            _bookRepository = bookRepository;
            _customerRepository = customerRepository;
        }

        [Route("Rent")]
        public IActionResult List()
        {
            // load all available books
            var availableBooks = _bookRepository.FindWithOwner(x => x.OccuId == 0);
            // check collection
            if (availableBooks.Count() == 0)
            {
                return View("Empty");
            }
            else
            {
                return View(availableBooks);
            }
        }

        public IActionResult RentProp(int bookId)
        {
            // load current book and all customers
            var lendVM = new RentViewModel()
            {
                Prop = _bookRepository.GetById(bookId),
                Tenants = _customerRepository.GetAll()
            };
            // Send data to the Lend view
            return View(lendVM);
        }

        [HttpPost]
        public IActionResult RentProp(RentViewModel lendViewModel)
        {
            // update the database 
            var book = _bookRepository.GetById(lendViewModel.Prop.PropertyId);

            var customer = _customerRepository.GetById(lendViewModel.Prop.PropertyId);

            book.BuRent = customer;

            _bookRepository.Update(book);

            // redirect to the list view
            return RedirectToAction("List");
        }
    }
}
