﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryManagement.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace LibraryManagement.Controllers
{
    public class VacateController : Controller
    {
        private readonly IPropRepository _propRepository;
        private readonly ITenantRepository _tenantRepository;

        public VacateController(IPropRepository propRepository, ITenantRepository tenantRepository)
        {
            _propRepository = propRepository;
            _tenantRepository = tenantRepository;
        }

        [Route("Vacate")]
        public IActionResult List()
        {
            // load all rent props
            var rentProps = _propRepository.FindWithOwnerAndRent(x => x.OccuId != 0);
            // Check the props collection
            if(rentProps == null || rentProps.ToList().Count() == 0)
            {
                return View("Empty");
            }
            return View(rentProps);
        }

        public IActionResult ReturnAProp(int propId)
        {
            // load the current prop
            var prop = _propRepository.GetById(propId);
            // remove borrower
            prop.BuRent = null;

            prop.OccuId = 0;
            // update database
            _propRepository.Update(prop);
            // redirect to list method
            return RedirectToAction("List");
        }
    }
}