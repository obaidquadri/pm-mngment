﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibraryManagement.Models;
using LibraryManagement.Data.Interfaces;
using LibraryManagement.ViewModel;

namespace LibraryManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPropRepository _propRepository;
        private readonly IOwnerRepository _ownerRepository;
        private readonly ITenantRepository _tenantRepository;

        public HomeController(IPropRepository propRepository,
                              IOwnerRepository ownerRepository,
                              ITenantRepository tenantRepository)
        {
            _propRepository = propRepository;
            _ownerRepository = ownerRepository;
            _tenantRepository = tenantRepository;
        }
        public IActionResult Index()
        {
            // create home view model
            var homeVM = new HomeViewModel()
            {
                OwnerCount = _ownerRepository.Count(x => true),
                TenantCount = _tenantRepository.Count(x=>true),
                PropCount = _propRepository.Count(x => true),
                RentPropCount= _propRepository.Count(x => x.BuRent != null)
            };
            // call view
            return View(homeVM);
        }
    }
}
