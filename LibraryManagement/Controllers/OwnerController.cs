﻿using LibraryManagement.Data.Interfaces;
using LibraryManagement.Data.Model;
using LibraryManagement.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace LibraryManagement.Controllers
{
    public class OwnerController : Controller
    {
        private readonly IOwnerRepository _ownerRepository;

        public OwnerController(IOwnerRepository ownerRepository)
        {
            _ownerRepository = ownerRepository;
        }

        [Route("Owner")]
        public IActionResult List()
        {
            var owners = _ownerRepository.GetAllWithProps();

            if (owners.Count() == 0) return View("Empty");

            return View(owners);
        }

        public IActionResult Update(int id)
        {
            var owner = _ownerRepository.GetById(id);

            if (owner == null) return NotFound();

            return View(owner);
        }

        [HttpPost]
        public IActionResult Update(Owner owner)
        {
            if (!ModelState.IsValid)
            {
                return View(owner);
            }

            _ownerRepository.Update(owner);

            return RedirectToAction("List");
        }

        public IActionResult Create()
        {
            var viewModel = new CreateOwnerViewModel
            { Referer = Request.Headers["Referer"].ToString() };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Create(CreateOwnerViewModel ownerVM)
        {
            if (!ModelState.IsValid)
            {
                return View(ownerVM);
            }

            _ownerRepository.Create(ownerVM.Owner);

            if (!String.IsNullOrEmpty(ownerVM.Referer))
            {
                return Redirect(ownerVM.Referer);
            }

            return RedirectToAction("List");
        }

        public IActionResult Delete(int id)
        {
            var owner = _ownerRepository.GetById(id);

            _ownerRepository.Delete(owner);

            return RedirectToAction("List");
        }
    }
}
