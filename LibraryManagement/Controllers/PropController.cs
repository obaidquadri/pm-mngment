﻿using LibraryManagement.Data.Interfaces;
using LibraryManagement.Data.Model;
using LibraryManagement.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Controllers
{
    public class PropController : Controller
    {
        private readonly IPropRepository _propRepository;
        private readonly IOwnerRepository _ownerRepository;

        public PropController(IPropRepository propRepository, IOwnerRepository ownerRepository)
        {
            _propRepository = propRepository;
            _ownerRepository = ownerRepository;
        }

        [Route("Property")]
        public IActionResult List(int? ownerId, int? rentId)
        {
            if (ownerId == null && rentId == null)
            {
                // show all props
                var props = _propRepository.GetAllWithOwner();
                // check props
                return CheckProps(props);
            }
            else if (ownerId != null)
            {
                // filter by author id
                var owner = _ownerRepository
                    .GetWithProps((int)ownerId);

                // check author props
                if (owner.Props.Count() == 0)
                {
                    return View("OwnerEmpty", owner);
                }
                else
                {
                    return View(owner.Props);
                }
            }
            else if (rentId != null)
            {
                // filter by borrower id
                var props = _propRepository
                    .FindWithOwnerAndRent(prop => prop.OccuId == rentId);
                // check borrower props
                return CheckProps(props);
            }
            else
            {
                // throw exception
                throw new ArgumentException();
            }
        }
        public IActionResult CheckProps(IEnumerable<Property> props)
        {
            if (props.Count() == 0)
            {
                return View("Empty");
            }
            else
            {
                return View(props);
            }
        }

        public IActionResult Create()
        {
            var propVM = new PropViewModel()
            {
                Owners = _ownerRepository.GetAll()
            };
            return View(propVM);
        }

        [HttpPost]
        public IActionResult Create(PropViewModel propViewModel)
        {
            if (!ModelState.IsValid)
            {
                propViewModel.Owners = _ownerRepository.GetAll();
                return View(propViewModel);
            }

            _propRepository.Create(propViewModel.Prop);

            return RedirectToAction("List");
        }

        public IActionResult Update(int id)
        {
            var propVM = new PropViewModel()
            {
                Prop = _propRepository.GetById(id),
                Owners = _ownerRepository.GetAll()
            };
            return View(propVM);
        }

        [HttpPost]
        public IActionResult Update(PropViewModel propViewModel)
        {
            if (!ModelState.IsValid)
            {
                propViewModel.Owners = _ownerRepository.GetAll();
                return View(propViewModel);
            }

            _propRepository.Update(propViewModel.Prop);

            return RedirectToAction("List");
        }

        public IActionResult Delete(int id)
        {
            var prop = _propRepository.GetById(id);

            _propRepository.Delete(prop);

            return RedirectToAction("List");
        }
    }
}
