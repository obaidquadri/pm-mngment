﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.ViewModel
{
    public class HomeViewModel
    {
        public int TenantCount { get; set; }
        public int OwnerCount { get; set; }
        public int PropCount { get; set; }
        public int RentPropCount { get; set; }
    }
}
