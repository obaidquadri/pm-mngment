﻿using LibraryManagement.Data.Model;

namespace LibraryManagement.ViewModel
{
    public class CreateOwnerViewModel
    {
        public Owner Owner { get; set; }
        public string Referer { get; set; }
    }
}
