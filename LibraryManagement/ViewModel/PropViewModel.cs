﻿using LibraryManagement.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.ViewModel
{
    public class PropViewModel
    {
        public Property Prop { get; set; }
        public IEnumerable<Owner> Owners { get; set; }
    }
}
