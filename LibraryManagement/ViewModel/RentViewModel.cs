﻿using LibraryManagement.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.ViewModel
{
    public class RentViewModel
    {
        public Property Prop { get; set; }
        public IEnumerable<Tenant> Tenants { get; set; }
    }
}
