﻿using LibraryManagement.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryManagement.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.Data.Repository
{
    public class OwnerRepository : Repository<Owner>, IOwnerRepository
    {
        public OwnerRepository(PMDbContext context) : base(context)
        {

        }
        
        public IEnumerable<Owner> GetAllWithProps()
        {
            return _context.Owners.Include(a => a.Props);
        }

        public Owner GetWithProps(int id)
        {
            return _context.Owners
                .Where(a => a.OwnerId == id)
                .Include(a => a.Props)
                .FirstOrDefault();
        }

      
    }
}
