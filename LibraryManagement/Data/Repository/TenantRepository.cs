﻿using LibraryManagement.Data.Interfaces;
using LibraryManagement.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Data.Repository
{
    public class TenantRepository : Repository<Tenant>, ITenantRepository
    {
        public TenantRepository(PMDbContext context) : base(context)
        {
        }
    }
}
