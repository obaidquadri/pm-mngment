﻿using LibraryManagement.Data.Interfaces;
using LibraryManagement.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Data.Repository
{
    public class PropRepository : Repository<Property>, IPropRepository
    {
        public PropRepository(PMDbContext context) : base(context)
        {
        }
                
        public IEnumerable<Property> FindWithOwner(Func<Property, bool> predicate)
        {
            return _context.Properties
                .Include(a => a.Owner)
                .Where(predicate);
        }

        public IEnumerable<Property> FindWithOwnerAndRent(Func<Property, bool> predicate)
        {
            return _context.Properties
                .Include(a => a.Owner)
                .Include(a => a.BuRent)
                .Where(predicate);
        }
             

        public IEnumerable<Property> GetAllWithOwner()
        {
            return _context.Properties.Include(a => a.Owner);
        }
    }
}
