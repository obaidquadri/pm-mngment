﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Data.Model
{
    public class Property
    {        
        public int PropertyId { get; set; }
        [Required, MinLength(3), MaxLength(50)]
        public string PName { get; set; }

        public virtual Owner Owner { get; set; }
        public int OwnerId { get; set; }

        public virtual Tenant BuRent { get; set; }
        public int OccuId { get; set; }
    }
}
