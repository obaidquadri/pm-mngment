﻿using LibraryManagement.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Data.Interfaces
{
    public interface IPropRepository : IRepository<Property>
    {
        IEnumerable<Property> GetAllWithOwner();
        IEnumerable<Property> FindWithOwner(Func<Property, bool> predicate);
        IEnumerable<Property> FindWithOwnerAndRent(Func<Property, bool> predicate);
    }
}
