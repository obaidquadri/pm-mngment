﻿using LibraryManagement.Data.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Data
{
    public static class DbInitializer
    {
        public static void Seed(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<PMDbContext>();


                // Add Tenants
                var syed = new Tenant { Name = "Syed Obaid" };

                var zaki = new Tenant { Name = "Zaki Quadri" };

                var adnan = new Tenant { Name = "Adnan  Sajid" };

                context.Tenants.Add(syed);
                context.Tenants.Add(zaki);
                context.Tenants.Add(adnan);

                // Add Owner
                var owner1 = new Owner
                {
                    Name = "Khalid Ahmed",
                    Props = new List<Property>()
                {
                    new Property { PName = "The Millionaire Fastlane" },
                    new Property { PName = "Fifth Minar Hotel" }
                }
                };
                var owner3 = new Owner
                {
                    Name = "Abdul Nayeem",
                    Props = new List<Property>()
                {
                    new Property { PName = "Dream Valley"},
                    
                }
                };

                var owner2 = new Owner
                {
                    Name = "Arif Quadri",
                    Props = new List<Property>()
                {
                    new Property { PName = "Bayt As Saleem"},                    
                    new Property { PName = "Rainbow 101"}
                }
                };

                context.Owners.Add(owner1);
                context.Owners.Add(owner2);
                context.Owners.Add(owner3);

                context.SaveChanges();
            }
        }
    }
}
